package com.example.demo;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.google.auth.oauth2.GoogleCredentials;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.messaging.FirebaseMessaging;

@SpringBootApplication
public class DemoFirebaseApplication {
	
	@Value("${fcm.type}")
	private String type;
	@Value("${fcm.project_id}")
	private String projectId;
	@Value("${fcm.private_key_id}")
	private String privateKeyId;
	@Value("${fcm.private_key}")
	private String privateKey;
	@Value("${fcm.client_email}")
	private String clientEmail;
	@Value("${fcm.client_id}")
	private String clientId;
	@Value("${fcm.auth_uri}")
	private String authUri;
	@Value("${fcm.token_uri}")
	private String tokenUri;
	@Value("${fcm.auth_provider_x509_cert_url}")
	private String authProviderX509CertUrl;
	@Value("${fcm.client_x509_cert_url}")
	private String clientX509CertUrl;

	public static void main(String[] args) {
		SpringApplication.run(DemoFirebaseApplication.class, args);
	}
	
	@Bean
	FirebaseMessaging firebaseMessaging() throws IOException {
		System.out.println("type"+type);
		/*
		 * GoogleCredentials googleCredentials = GoogleCredentials.fromStream(new
		 * ClassPathResource("firebase-service-account.json").getInputStream());
		 */
		 InputStream targetStream = new ByteArrayInputStream(getFirebaseCredentialsJson().getBytes());
		 GoogleCredentials googleCredentials = GoogleCredentials.fromStream(targetStream);
		
	    FirebaseOptions firebaseOptions = FirebaseOptions
	            .builder()
	            .setCredentials(googleCredentials)
	            .build();
	    FirebaseApp app = FirebaseApp.initializeApp(firebaseOptions, "my-app");
	    return FirebaseMessaging.getInstance(app);
	}
	
	private String getFirebaseCredentialsJson() {
		String json="{"
				+ "  \"type\": \""+type+"\","
				+ "  \"project_id\": \""+projectId+"\","
				+ "  \"private_key_id\": \""+privateKeyId+"\","
				+ "  \"private_key\": \""+privateKey+"\","
				+ "  \"client_email\": \""+clientEmail+"\","
				+ "  \"client_id\":\""+clientId+"\","
				+ "  \"auth_uri\": \""+authUri+"\","
				+ "  \"token_uri\": \""+tokenUri+"\","
				+ "  \"auth_provider_x509_cert_url\": \""+authProviderX509CertUrl+"\","
				+ "  \"client_x509_cert_url\": \""+clientX509CertUrl+"\""
				+ "}";
		System.out.println(json);
		
		return json;
	}

}
