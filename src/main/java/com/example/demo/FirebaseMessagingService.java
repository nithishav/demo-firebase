package com.example.demo;

import java.util.List;

import org.springframework.stereotype.Service;

import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.FirebaseMessagingException;
import com.google.firebase.messaging.Message;
import com.google.firebase.messaging.Notification;
import com.google.firebase.messaging.TopicManagementResponse;

@Service
public class FirebaseMessagingService {
	
	 private final FirebaseMessaging firebaseMessaging;

	    public FirebaseMessagingService(FirebaseMessaging firebaseMessaging) {
	        this.firebaseMessaging = firebaseMessaging;
	    }


	    public String sendNotification(Note note, String token) throws FirebaseMessagingException {

	        Notification notification = Notification
	                .builder()
	                .setTitle(note.getSubject())
	                .setBody(note.getContent())
	                .build();

	        Message message = Message
	                .builder()
	                .setTopic(token)
	                //.setToken(token)
	                .setNotification(notification)
	                .putAllData(note.getData())
	                .build();

	        return firebaseMessaging.send(message);
	    }
	    
	    
	    public void receiveNotification(String topic) throws FirebaseMessagingException {

	    	List<String> registrationTokens=null;
			// Subscribe the devices corresponding to the registration tokens to the
	    	// topic.
	    	TopicManagementResponse response = FirebaseMessaging.getInstance().subscribeToTopic(registrationTokens, topic);
	    	//firebaseMessaging.su
	    	// See the TopicManagementResponse reference documentation
	    	// for the contents of response.
	    	System.out.println(response.getSuccessCount() + " tokens were subscribed successfully");
	    }


}
